# coding: utf8

"""识别语音"""

import numpy as np
from asr.psf.base import logfbank
from aip import AipSpeech
from asr.configs import RATE, CUID, DEV_PID, AUDIO_OUTPUT, TokenPath, ModelFile
from yiwa.log import Log

import torch
import torchaudio

from asr.asr_abc.conformer import Conformer
from asr.asr_abc.utils import load_dict, remove_repeated_and_leq

logger = Log().logger

"""读取wav音频文件并返回"""


def get_file_content(filePath):
    with open(filePath, 'rb') as fp:
        return fp.read()


"""
读取paudio录制好的音频文件, 调用百度语音API, 设置api参数, 完成语音识别
    client:AipSpeech对象
    afile:音频文件
    afmt:音频文件格式(wav)
"""


def aip_get_asrresult(client: AipSpeech, afile, afmt):
    """百度aip,获取识别结果
    报错信息参考百度: https://cloud.baidu.com/doc/SPEECH/s/Dk4o0bmkl
    """
    print("Waiting...")
    try:
        # 识别结果已经被SDK由JSON字符串转为dict
        result = client.asr(get_file_content(afile), afmt, RATE,
                            {"cuid": CUID, "dev_pid": DEV_PID})
        # 如果err_msg字段为"success."表示识别成功, 直接从result字段中提取识别结果, 否则表示识别失败
        if result["err_msg"] == "success.":
            return result["result"]
        logger.error(f"百度语音识别 失败 {result}")
        return []
    except Exception as e:
        logger.error(f"百度语音识别 报错 {e}")
        return []


ConformerModel = Conformer(
    num_features=80,
    num_classes=4336,
    nhead=4,
    d_model=512,
)
TorchDevice = torch.device("cpu")
checkpoint = torch.load(ModelFile, map_location="cpu")
ConformerModel.load_state_dict(checkpoint["model"], strict=True)
del checkpoint
ConformerModel.eval()
TokenDict = load_dict(TokenPath)


def asr_abc_result():
    audio_samples, sample_rate = torchaudio.load(AUDIO_OUTPUT)
    features = logfbank(audio_samples, sample_rate, nfilt=80)
    features = features.astype(np.float32)
    features = torch.tensor(features).to(TorchDevice).unsqueeze(0)
    with torch.no_grad():
        nnet_output, _ = ConformerModel(features)
    token_ids = nnet_output.argmax(dim=2)
    token_ids = token_ids.cpu().squeeze().numpy()
    token_ids = remove_repeated_and_leq(token_ids)
    return [TokenDict[id] for id in token_ids]


if __name__ == "__main__":
    print(asr_abc_result())
