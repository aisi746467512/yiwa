# coding: utf8

"""数据库操作
感兴趣的可以写成单例模式;
可以用其他orm, 比如我喜欢orator-orm.com;
你可以用流行的SqlAlchemy(去写一堆model)"""

import os
import sqlite3
from yiwa.settings import BASE_DIR
from yiwa.log import Log
from yiwa.memory import SystemCache

logger = Log().logger


class Sqlite3DB(object):
    """SQLite操作类"""

    def __init__(self, db_file=os.path.join(BASE_DIR, "yiwa.s3db")):
        super(Sqlite3DB, self).__init__()
        self.db_file = db_file
        self.conn = None
        self.cursor = None

    def connect(self):
        self.conn = sqlite3.connect(self.db_file, timeout=10)
        # 返回结果采用dict类型
        # https://blog.csdn.net/zhengxiaoyao0716/article/details/50278069
        self.conn.row_factory = lambda cursor, row: dict(
            (col[0], row[idx]) for idx, col in enumerate(cursor.description))
        self.cursor = self.conn.cursor()

    def disconnect(self):
        self.cursor.close()
        self.conn.close()

    def select(self, sql, as_dict=False):
        try:
            self.connect()
            self.cursor.execute(sql)
            result = self.cursor.fetchall()
            return result if as_dict \
                else [tuple(res.values()) for res in result]
        except Exception as error:
            logger.error(error)
            return []
        finally:
            self.disconnect()

    def select_one(self, sql, as_dict=True):
        result = self.select(sql, as_dict)
        return result[0] if result else {}

    def execute(self, sql, result_set=False):
        try:
            self.connect()
            self.cursor.execute(sql)
            self.conn.commit()
            return self.cursor.lastrowid if result_set else True
        except Exception as error:
            try:
                # 多sql语句拼接
                self.cursor.executescript(sql)
                self.conn.commit()
                return True
            except Exception as ex:
                logger.error(f"{error} {ex}")
                return False
        finally:
            self.disconnect()

    def executemany(self, sql, data):
        try:
            self.connect()
            self.cursor.executemany(sql, data)
            self.conn.commit()
            return True
        except Exception as error:
            logger.error(error)
            return False
        finally:
            self.disconnect()


class DataConveyor(object):
    """yiwa数据交互类，用于页面顶部状态的更新。"""

    def __init__(self):
        super(DataConveyor, self).__init__()
        self.init_captions = {
            0: "请唤醒我吧：",
            1: "请说出指令吧：",
            -1: "我出错了，尝试重启机器吧！",
        }
        self.s3db = Sqlite3DB()
        self.sc = SystemCache()

    def _init(self):
        sql = "SELECT id FROM yiwa;"
        if self.s3db.select(sql):
            self.sleep()
        else:
            sql_init_insert = f"""INSERT INTO yiwa(status, caption, listening, info, stt, cur_appid) 
                VALUES(0, "{self.init_captions.get(0)}", 0, "待唤醒", "", "")
                """
            self.s3db.execute(sql_init_insert)

    def refresh(self, _apps, _commands):
        self.s3db.execute("DELETE FROM apps")
        self.s3db.execute("DELETE FROM commands")
        self.s3db.execute("UPDATE sqlite_sequence SET seq=0 WHERE name='apps';")
        self.s3db.execute("UPDATE sqlite_sequence SET seq=0 WHERE name='commands';")

        insert_sql_apps = "INSERT INTO apps(appid, appname) VALUES(?,?)"
        self.s3db.executemany(insert_sql_apps, _apps)

        insert_sql_commands = """INSERT INTO commands(name, commands, action, global, appid) 
            VALUES(?, ?, ?, ?, ?)"""
        self.s3db.executemany(insert_sql_commands, _commands)

    def sleep(self):
        self.sc.set_yiwa_table(status=0,
                               caption=self.init_captions.get(0),
                               listening=0,
                               stt="睡眠待命")
        sql = f"""UPDATE yiwa SET status = 0,
            caption="{self.init_captions.get(0)}",
            listening=0,
            stt="睡眠待命"
            """
        self.s3db.execute(sql)

    def wakeup(self):
        self.sc.set_yiwa_table(status=1,
                               caption=self.init_captions.get(1),
                               stt="^_^ 唤醒成功")
        sql = f"""UPDATE yiwa SET status = 1,
            caption="{self.init_captions.get(1)}",
            stt="^_^ 唤醒成功"
            """
        self.s3db.execute(sql)

    def access(self, command):
        """命中指令状态更新"""
        self.sc.set_yiwa_table(status=1,
                               caption=self.init_captions.get(1),
                               stt=f"指令命中: {command}")
        sql = f"""UPDATE yiwa SET status = 1,
            caption="{self.init_captions.get(1)}",
            stt="指令命中: {command}"
            """
        self.s3db.execute(sql)

    def update_cur_appid(self, appid):
        """更新当前所在app的id"""
        sql = f"""UPDATE yiwa SET cur_appid="{appid}";"""
        self.s3db.execute(sql)

    def cur_appid(self):
        """获取当前所在app的id"""
        sql = f"SELECT cur_appid FROM yiwa LIMIT 1;"
        data = self.s3db.select(sql, as_dict=True)
        return data[0].get("cur_appid") if data else ""

    def error(self):
        self.sc.set_yiwa_table(status=-1,
                               caption=self.init_captions.get(-1),
                               stt="接收语音发送错误")
        sql = f"""UPDATE yiwa SET status = -1,
            caption="{self.init_captions.get(-1)}",
            stt="接收语音发送错误"
            """
        self.s3db.execute(sql)

    def stt(self, command):
        """语音识别结果更新"""
        if not command:
            command = "-"
        self.sc.set_yiwa_table(stt=command)
        sql = f"""UPDATE yiwa SET stt="{command}";"""
        self.s3db.execute(sql)

    def info(self, info):
        if not info:
            info = "-"
        self.sc.set_yiwa_table(info=info)
        sql = f"""UPDATE yiwa SET info="{info}";"""
        self.s3db.execute(sql)

    def listening(self, info="录音中"):
        """监听中"""
        self.sc.set_yiwa_table(listening=1, info=info, stt="-")
        sql = f"""UPDATE yiwa SET listening = 1,
            info="{info}",
            stt="-"
            """
        self.s3db.execute(sql)

    def listened(self, info="录音结束"):
        """监听完毕"""
        self.sc.set_yiwa_table(listening=0, info=info)
        sql = f"""UPDATE yiwa SET listening = 0,
            info="{info}"
            """
        self.s3db.execute(sql)

    def all_command(self):
        """所有全局命令和当前功能的子命令"""
        cur_appid = self.cur_appid()
        and_global = "AND global=1"
        if cur_appid:
            and_global = f"""AND (global=1 OR appid="{cur_appid}")"""
        sql = f"""SELECT commands, action, appid FROM commands 
            WHERE 1=1 {and_global}
            ORDER BY hot DESC"""
        return self.s3db.select(sql)

    def filter_command(self, command):
        """筛选全局命令和当前功能的子命令"""
        cur_appid = self.cur_appid()
        and_global = "AND global=1"
        if cur_appid:
            and_global = f"""AND (global=1 OR appid="{cur_appid}")"""
        sql = f"""SELECT commands, action, appid FROM commands 
          WHERE commands LIKE "%{command}%" {and_global} 
          ORDER BY hot DESC"""
        return self.s3db.select(sql)

    def filter_command_by_actions(self, actions):
        if not actions:
            return []
        in_actions = "','".join(actions)
        sql = f"""SELECT commands, action, appid FROM commands 
          WHERE action IN ('{in_actions}') 
          ORDER BY hot DESC"""
        return self.s3db.select(sql)

    def hot(self, action):
        """增加动作的热度"""
        sql = f"""UPDATE commands SET hot=hot+1 
            WHERE action="{action}"
            """
        self.s3db.execute(sql)
