
-- 自律项
CREATE TABLE "options" (
	 "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	 "name" TEXT,   -- 自律项名称
	 "order" integer,   -- 自律项排序
	 "image_path" text, -- 图片路径
	 "enable" integer DEFAULT 1 -- 是否启用
)

-- 鼓励分值
CREATE TABLE "scores" (
	 "id" INTEGER NOT NULL,
	 "score" integer,   -- 对应分值
	 "name" TEXT,   -- 鼓励名
	 "image_path" TEXT, -- 图片路径
	 "enable" integer DEFAULT 1,  -- 是否有效
	PRIMARY KEY("id")
)

-- 每日记录
CREATE TABLE "day" (
	 "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	 "option" integer,  -- 记录项id
	 "createtime" DATETIME, -- 添加时间
	 "score" integer,   -- 对应鼓励分值项id
	 "date" DATE,   -- 日期
	 "week" integer -- 周数（0~6，对应周一~周日）
)

-- 奖项
CREATE TABLE "awards" (
	 "id" INTEGER NOT NULL,
	 "name" TEXT,   -- 奖项名称
	 "score" integer,   -- 奖项所需分数
	 "order" integer,   -- 奖项排序
	 "image_path" TEXT, -- 奖项图片路径
	 "got" integer, -- 是否取得, 默认0: 没有
	 "got_time" datetime,   -- 取得时间
	 "enable" integer DEFAULT 1,  -- 是否有效
	PRIMARY KEY("id")
)

-- 兑换记录
CREATE TABLE "exchange_records" (
	 "id" INTEGER(11,0) NOT NULL,
	 "balance" integer(4,0),    -- 余额（只有last=1才有余额值，其他行无balance）
	 "last" integer DEFAULT 0,  -- 是最后一行兑换统计结果行（1是，0否，最后一行）
	 "award" text,  -- 兑换奖项
	 "number" INTEGER(4,0), -- 兑换数量
	 "time" datetime,   -- 兑换时间
	PRIMARY KEY("id")
)

-- 宝贝信息, 目前仅支持一个
CREATE TABLE "baby" (
	 "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	 "name" TEXT,
	 "birthday" date,
	 "enable" integer DEFAULT 1
)