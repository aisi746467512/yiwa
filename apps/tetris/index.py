# coding: utf8

"""页面"""

import time
from flask import render_template
from apps import app
from apps.__public._memcache import Memcache

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains

from util.string import StringUtil


@app.route("/tetris/index")
def index():
    return render_template("tetris/index.html")


def start(driver: WebDriver):
    """开始"""
    driver.find_element_by_id("startPause").click()


def restart(driver: WebDriver):
    """重来"""
    driver.find_element_by_id("reset").click()


def send_keys(driver: WebDriver, key: str):
    say = Memcache().say()
    number = StringUtil.chn_number(say)
    for _ in range(number):
        ActionChains(driver).send_keys(key).perform()
        time.sleep(0.5)


def left(driver: WebDriver):
    """左移"""
    send_keys(driver, "A")


def right(driver: WebDriver):
    """右移"""
    send_keys(driver, "D")


def rotate(driver: WebDriver):
    """旋转"""
    send_keys(driver, "K")


def fall(driver: WebDriver):
    """下落"""
    send_keys(driver, "W")
